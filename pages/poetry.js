import React from "react";
import { useState } from 'react';
import ContainerBlock from "../components/ContainerBlock";

import { makeStyles } from "@material-ui/core/styles";

import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";

import kavanasJSON from "constants/kavana";
import Paginations from "components/Pagination/Pagination.js";
import AboutPoet from "components/AboutPoet";

const styles = {
    cardTitle: {
        color: "blue",
        fontSize: "x-large",
        fontWeight: "bold"
    },
    cardContent: {
        fontSize: "large",
    },
    textCenter: {
        textAlign: "center"
    },
    textRight: {
        textAlign: "right"
    },
    textLeft: {
        textAlign: "left"
    },
    fontFamily: {
        fontFamily: "'Akaya Kanadaka', cursive"
    },
    header: {
        textAlign: "center",
        fontSize: "xx-large"
    },
    cardContainer: {
        display: "grid",
        gridTemplateColumns: "1fr 1fr"
    },
    cardColumn: {
        float: "left",
        width: "90%",
    }
};
function shuffle(array) {
    let currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }

    return array;
}



const useStyles = makeStyles(styles);


export default function projects() {
    const classes = useStyles();
    // shuffle(kavanasJSON);

    const [kavanas, setKavanas] = useState(kavanasJSON.slice(0, 10));
    const [kavanasIndex, setKavanasIndex] = useState(0);

    let cards = kavanas.map((data, i) => {
        let d = data.text.split('\n');
        let b = d.map((d, j)=> {
            if (d) return (<p className={[classes.cardContent, classes.textLeft].join(" ")} key={j}>{d}</p>)
            else return (<br />)
        })

        return <Card className={classes.cardColumn} key={i}>
            <CardBody className={classes.textLeft}>
                <h4 className={classes.cardTitle}>{data.header ? data.header : d[0]}</h4>
                {b}
            </CardBody>
        </Card>
    })

    let pageClickAction = function (page) {
        if (kavanasIndex > kavanasJSON.length - 1 || kavanasIndex < 0) setKavanasIndex((0));
        if (page == 'NEXT') {
            if ((kavanasIndex + 1) > (kavanasJSON.length - 1)) {
                setKavanas(kavanasJSON.slice(0, 10));
                setKavanasIndex(10);
            }
            else {
                setKavanas(kavanasJSON.slice(kavanasIndex + 1, kavanasIndex + 11));
                setKavanasIndex(kavanasIndex + 11);
            }
        }
        else {
            if ((kavanasIndex - 11) <= (0)) {
                setKavanas(kavanasJSON.slice((kavanasJSON.length - 11), (kavanasJSON.length - 1)));
                setKavanasIndex((kavanasJSON.length - 11));
            }
            else {
                setKavanas(kavanasJSON.slice((kavanasIndex - 11), (kavanasIndex - 1)));
                setKavanasIndex((kavanasIndex - 11));
            }
        }
    }

    return (
        <ContainerBlock title="Projects - Sandeep Bashetti">
            <section className="bg-white dark:bg-gray-800">
                <div class="max-w-6xl mx-auto bg-white dark:bg-gray-800">
                    <AboutPoet />

                    <h1 className={[classes.fontFamily, classes.header].join(" ")} >ನನ್ನ ಕವನಗಳು</h1>
                    <Paginations
                        pages={[
                            { text: "PREV" },
                            { text: "NEXT" }
                        ]}
                        color="info"
                        pageClicked={pageClickAction}
                    />
                    <div className={[classes.fontFamily, classes.textCenter, classes.cardContainer].join(" ")}>
                        {cards}
                    </div>
                </div>

            </section>
        </ContainerBlock>
    );
}
