import React from "react";
import userData from "@constants/data";

import { makeStyles } from "@material-ui/core/styles";

const styles = {
    cardTitle: {
        color: "blue",
        fontSize: "x-large",
        fontWeight: "bold"
    },
    cardContent: {
        fontSize: "large",
    },
    textCenter: {
        textAlign: "center"
    },
    textRight: {
        textAlign: "right"
    },
    textLeft: {
        textAlign: "left"
    },
    fontFamily: {
        fontFamily: "'Akaya Kanadaka', cursive"
    },
    header: {
        textAlign: "center",
        fontSize: "xx-large"
    }
};

const useStyles = makeStyles(styles);


export default function Hero() {
  const colors = ["#F59E0B", "#84CC16", "#10B981", "#3B82F6"];
  const classes = useStyles();
  return (
    <div className="flex flex-row justify-center items-start overflow-hidden">
      {/* Text container */}

      <div className="w-full md:w-1/2 mx-auto text-center md:text-left lg:p-20">
       <h1 className={[classes.fontFamily, classes.header].join(" ")}>ಕವಿ ಪರಿಚಯ</h1>
       <p><b>ಹೆಸರು</b> : ಸಂದೀಪ್ ಬಶೆಟ್ಟಿ</p>
       <p><b>ವಿಳಾಸ</b> : ಬೆಳಗಾವಿ ಜಿಲ್ಲೆಯ ಹುಕ್ಕೇರಿ ತಾಲೂಕಿನ ಅತ್ತಿಹಾಳ್ ಗ್ರಾಮ</p>
       <br/>
       <br/>
       <p><b>ನನ್ನ ಕವನಗಳು</b> ಭಾಷಾಂತರಿಸಲಾಗದ ಭಾವನೆಗಳು, ಕಾಣದ ಕನಸಿನ ದೃಶ್ಯ ಸಂಕಲನಗಳು, ಕಸ್ತೂರಿ ಕನ್ನಡದ ಕಾವ್ಯ ಕಂಪನಗಳು.</p>
      </div>

      {/* Image container */}
      <div className="hidden lg:block relative w-full md:w-1/2 -mr-40 mt-20">
        <div className="w-3/4 ">
          <img src={userData.avatarUrl2} alt="avatar" />
          <div className="flex flex-row justify-between mt-4">
            <div className="flex flex-row space-x-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-arrow-90deg-up"
                viewBox="0 0 16 16"
              >
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
