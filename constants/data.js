const userData = {
  githubUsername: "SandeepAB",
  name: "Sandeep Bashetti",
  designation: "Senior Software Developer (backend)",
  avatarUrl: "/sandeep.png",
  avatarUrl2: "/sandeep2.png",
  email: "sandeepbashetti4@gmail.com",
  phone: "+91 7829729822",
  address: "Belgaum, Karnataka, India.",
  projects: [
    {
      title: "Rashmi Electricals",
      link: "https://rashmielectricals-90dd0.firebaseapp.com/",
      imgUrl: "/re.png",
    },
    {
      title: "Jatka Eats",
      link: "https://github.com/SandeepAB/jatka-eats",
      imgUrl: "/je.png",
    },
    {
      title: "Happy Delivery",
      link: "/",
      imgUrl: "/hd.jpeg",
    }
  ],
  about: {
    title: "Senior Software Engineer",
    company: "Falabella",
    year: "2022",
    companyLink: "https://www.falabella.com/falabella-cl",
    description: ["Senior developer at stores team"],
    // description: []
  },
  experience: [
    {
      title: "Senior Software Engineer",
      company: "Falabella",
      year: "2022",
      companyLink: "https://www.falabella.com/falabella-cl",
      desc: "Senior developer at stores team",
    },
    {
      title: "Senior Software Engineer",
      company: "EnterpriseBot",
      year: "2021",
      companyLink: "https://enterprisebot.ai/",
      desc: "Software developer for product Blitzico.",
    },
    {
      title: "Engineer",
      company: "Brillio",
      year: "2018",
      companyLink: "https://www.brillio.com",
      desc: "R & D Engineer at creative labs",
    }
  ],
  resumeUrl:
    "https://drive.google.com/file/d/1PSCF42GzAo0453MiLqxgwdbxirlqSe65/view?usp=sharing",
  socialLinks: {
    instagram: "https://instagram.com/sandeep.ab20",
    twitter: "https://twitter.com/",
    linkedin: "https://linkedin.com/in/sandeep-bashetti-332044116",
    github: "https://github.com/SandeepAB",
    facebook: "https://facebook.com/",
  },
};

export default userData;
